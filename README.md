# Crypto gui
![Crypto-gui](icon.png)  
This program showcases and runs various algorithms from the field of mathematical cryptography.  
Input arguments can be written as python mathematical expressions, try it out ;)

### Prerequisites
```
Python 3
Gmpy2 for python 3
Gtk version 3
Python GObject
```
Depending on your distribution, package names may differ a bit.
Under arch linux it's as follows:
```
python
python-gmpy2
gtk3
pygobject-devel
python-gobject
```

### Running the program
```
python main.py
```
### Screenshot
![screenshot](screenshot.png)
