import argparse
from math import ceil

#buffered print function to speed up printing to stdout
def buffered_print(i):
    global k, buffer_size, print_buffer
    if k != buffer_size:
        print_buffer[k] = str(i)+"\n"
        k += 1
        return
    k = 0
    print(''.join(print_buffer))
    print_buffer = [0]*buffer_size

#a simple sieve of eratosthenes
def sieve_method(n, to_cmd = False):
    a = [True for i in range(0, n+1)]
    for i in range(2, int(n**0.5)+1):
        if a[i]:
            #for j in range(i**2, n+1, i):
                #a[j] = False
            #pythonic way is a bit faster! :)
            a[i**2::i] = [False]*ceil((n+1-(i**2))/i)
    result = [2] + [i for i in range(3,n+1,2) if a[i]]
    if to_cmd:
        for num in result:
            buffered_print(num)
    else:
        primes = [None]*len(result)
        for i in range(0,len(result)-1):
            primes[i] = str(result[i])+"\n"
        primes[-1] = str(result[-1])
        return ''.join(primes)

#pretty fast method found on stack overflow
def rwh_primes2(n, to_cmd = False):
    # https://stackoverflow.com/questions/2068372/fastest-way-to-list-all-primes-below-n-in-python/3035188#3035188
    """ Input n>=6, Returns a list of primes, 2 <= p < n """
    n += 1
    correction = (n%6>1)
    n = {0:n,1:n-1,2:n+4,3:n+3,4:n+2,5:n+1}[n%6]
    sieve = [True] * (n//3)
    sieve[0] = False
    for i in range(int(n**0.5)//3+1):
      if sieve[i]:
        k=3*i+1|1
        sieve[      ((k*k)//3)      ::2*k]=[False]*((n//6-(k*k)//6-1)//k+1)
        sieve[(k*k+4*k-2*k*(i&1))//3::2*k]=[False]*((n//6-(k*k+4*k-2*k*(i&1))//6-1)//k+1)
    result = [2,3] + [3*i+1|1 for i in range(1,n//3-correction) if sieve[i]]
    if to_cmd:
        #for num in result: print(num)
        for num in result: buffered_print(num)
        return
    else:
        primes = [None]*len(result)
        for i in range(0,len(result)-1):
            primes[i] = str(result[i])+"\n"
        primes[-1] = str(result[-1])
        return ''.join(primes)


#function to check for negative int arguments
def check_positive(value):
    ivalue = int(value)
    if ivalue < 2:
         raise argparse.ArgumentTypeError("The number has to be an integer greater than 2")
    return ivalue

#what to do if the file is run as a script independently
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description = "Printing primes using a simple sieve of Eratosthenes")
    parser.add_argument('N', metavar='N', type=check_positive, help='the number until which to print primes')
    args = parser.parse_args()
    k = 0
    buffer_size = 60000
    if args.N < buffer_size:
        buffer_size = args.N
    print_buffer = [0]*buffer_size
    #sieve_method(args.N, True)
    rwh_primes2(args.N, True)
    print_buffer = ''.join([print_buffer[i] for i in range(0,buffer_size) if print_buffer[i] != 0])
    print_buffer = print_buffer[0:-1] + print_buffer[-1][0:-1]
    print(print_buffer)
