import argparse
try:
	from scripts.baillie_PSW import baillie_psw_test
except ModuleNotFoundError:
	from baillie_PSW import baillie_psw_test
from gmpy2 import is_square

def legendre_symbol(a, p, to_cmd = False):
	if to_cmd:
		if not baillie_psw_test(p): return str(p)+" is not a prime number."
	res = pow(a, (p-1)//2, p) 
	if res == 0 or res == 1:
		return res
	else:
		return -1
		

#function to check for negative int arguments
def check_positive(value):
	ivalue = int(value)
	if ivalue < 0:
		 raise argparse.ArgumentTypeError("%s is an invalid positive int value" % value)
	return ivalue

#what to do if the file is run as a script independently
if __name__ == "__main__":
	parser = argparse.ArgumentParser(description = "Compute the legendre symbol for\
		an integer a and a prime p")
	parser.add_argument('a', metavar='a', type=check_positive, help='an integer a')
	parser.add_argument('p', metavar='p', type=check_positive, help='a prime p')
	args = parser.parse_args()
	result = legendre_symbol(args.a, args.p, True)
	print(result)
