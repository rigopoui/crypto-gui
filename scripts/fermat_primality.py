import random
import argparse
import gmpy2
from gmpy2 import mpz,mpq,mpfr,mpc
#inputs: n - the number to test for primality
#        k - the amount of times to test for primality
def fermat_test(n, k):
    # Implementation uses the Fermat Primality Test
    # If number is even, it's a composite number
    n = mpz(n)
    if n == 2 or n == 1:
        return True
    if n % 2 == 0:
        return False
    for i in range(k):
        a = mpz(random.randint(2, n-2))
        if gmpy2.powmod(a, n-1, n) != 1:
            return False
    return True

#function to check for negative int arguments
def check_positive(value):
    ivalue = int(value)
    if ivalue < 0:
         raise argparse.ArgumentTypeError("%s is an invalid positive int value" % value)
    return ivalue
    
#what to do if program is run independently as a script    
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description = "Implementation of Fermat's primality test")
    parser.add_argument('n', metavar='n', type=check_positive, help='the number to test for primality')
    parser.add_argument('k', metavar='k', type=check_positive, help='the amount of times to test for primality')
    args = parser.parse_args()
    result = fermat_test(args.n, args.k)
    print(result)