try:
    from scripts.baillie_PSW import baillie_psw_test
except ModuleNotFoundError:
    from baillie_PSW import baillie_psw_test
import argparse
from random import randint
import gmpy2
from gmpy2 import mpz,mpq,mpfr,mpc

def func(x, b, N):
	return gmpy2.f_mod((x*x + b), N)

def func2(x, N):
	return gmpy2.f_mod((x*x + 1), N)

def find_factor_with_rand(N, k):
	while k != 0:
		b = randint(1, N-3)
		s = randint(0, N-1)
		A = s
		B = s
		g = 1
		while g == 1:
			A = func(A, b, N)
			B = func(func(B, b, N), b, N)
			g = gmpy2.gcd(A - B, N)
		if g != N and baillie_psw_test(g):
			return int(g)
		k -= 1
	return None

def find_factor(N, k):
	k_fixed = k
	while k != 0:
		A = 2
		B = 2
		g = 1
		while g == 1:
			A = func2(A, N)
			B = func2(func2(B, N),  N)
			g = gmpy2.gcd(A - B, N)
		if g != N and baillie_psw_test(g):
			return int(g)
		k -= 1
	#the method without randint is faster but fails more often, so if it fails
	#the method using randint is called
	return find_factor_with_rand(N, k_fixed)

#n a positive odd composite integer to factor
def pollard_rho_factorization(N, k):
	#the list of our prime decomposition
	decomp_list = []
	N = mpz(N)
	if N < 4:
		return [N]
	#special cases, if N is prime
	if baillie_psw_test(N):
		return [int(N)]
	#and getting rid of all the factors of 2 because 
	#that seems to hang the program sometimes when the input is an even number
	while gmpy2.f_mod(N, 2) == 0:
		decomp_list.append(2)
		N = gmpy2.f_div(N, 2)
	if N == 1:
		return decomp_list
	while not baillie_psw_test(N):
		factor = find_factor(N, k)
		if factor == None:
			return None
		#disable continued execution, to test complexity 
		#of finding just one factor using this method
		#else:
			#return 0
		decomp_list.append(factor)
		N = gmpy2.f_div(N, factor)
	decomp_list.append(int(N))
	return decomp_list

def is_power_of_two(x):
    return (x != 0) and ((x & (x - 1)) == 0);


#function to check for negative int arguments
def check_positive(value):
    ivalue = int(value)
    if ivalue < 0:
         raise argparse.ArgumentTypeError("%s is not a valid positive integer" % value)
    return ivalue

#what to do if the file is run as a script independently
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description = "Pollard's rho factorization method")
    parser.add_argument('n', metavar='n', type=check_positive, help='The positive composite odd number to factor')
    parser.add_argument('k', metavar='k', type=check_positive, help='How many times to try before giving up')
    args = parser.parse_args()
    result = pollard_rho_factorization(args.n, args.k)
    print(result)
