try:
    from scripts.baillie_PSW import baillie_psw_test
except ModuleNotFoundError:
    from baillie_PSW import baillie_psw_test
import argparse

def prime_intervals(A, B, to_cmd = False):
	
	if A > B:
		A, B = B, A
		#print("A was larger than B so they have been swapped")
	if A % 2 ==0:
		A += 1
	primes = []
	if A < 2 and B >= 2:
		primes.append("2\n")
		if to_cmd:
			print(2)
	if to_cmd:
		for i in range(A, B+1, 2):
			if baillie_psw_test(i):
				print(i)
	else:
		#I thought this way would be a bit faster but it doesn't make a difference
		#primes = [str(i)+"\n" for i in range(A,B+1,2) if baillie_psw_test(i)]
		for i in range(A, B+1, 2):
			if baillie_psw_test(i):
				primes.append(str(i)+"\n")
		if len(primes) > 0:
			primes[-1] = primes[-1][0:-1]
		return ''.join(primes)

#function to check for negative int arguments
def check_positive(value):
	ivalue = int(value)
	if ivalue < 0:
		 raise argparse.ArgumentTypeError("%s is an invalid positive int value" % value)
	return ivalue

#what to do if the file is run as a script independently
if __name__ == "__main__":
	parser = argparse.ArgumentParser(description = "Printing primes using baille_PSW testing")
	parser.add_argument('A', metavar='A', type=check_positive, help='the number from which to print primes')
	parser.add_argument('B', metavar='B', type=check_positive, help='the number until which to print primes')
	args = parser.parse_args()
	prime_intervals(args.A, args.B, True)