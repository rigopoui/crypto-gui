import gmpy2
from gmpy2 import mpz,mpq,mpfr,mpc
def continued_fraction(p, q):
    """Returns the finite continued fraction of p/q as a list."""
    cf = []
    p = mpz(p)
    q = mpz(q)
    orig_p = p
    orig_q = q
    while q:
        div = gmpy2.f_div(p, q)
        cf.append(str(p)+"/"+str(q)+" = "+str(div)+" + "+str(p - div*q)+"/"+str(q)+"\n")
        p, q = q, gmpy2.f_mod(p, q)
    cf.append(str(continued_fraction_terms_only(orig_p, orig_q)))
    return ''.join(cf)

def continued_fraction_terms_only(p, q):
    """Returns the finite continued fraction of p/q as a list."""
    cf = []
    p = mpz(p)
    q = mpz(q)
    while q != 0:
        cf.append(int(gmpy2.f_div(p , q)))
        p, q = q, gmpy2.f_mod(p, q)
    return cf


#what to do if the file is run as a script independently
if __name__ == "__main__":
    import argparse

    def check_positive(value):
        ivalue = int(value)
        if ivalue < 0:
             raise argparse.ArgumentTypeError("%s is an invalid positive int value" % value)
        return ivalue

    parser = argparse.ArgumentParser(description = "Prints the terms of a continued fraction")
    parser.add_argument('p', metavar='p', type=check_positive, help='The numerator')
    parser.add_argument('q', metavar='q', type=check_positive, help='The denominator')
    args = parser.parse_args()
    result = continued_fraction_terms_only(args.p, args.q)
    #result = continued_fraction(args.p, args.q)    
    print(result)
