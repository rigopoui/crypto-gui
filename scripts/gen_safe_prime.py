try:
    from scripts.baillie_PSW import baillie_psw_test
except ModuleNotFoundError:
    from baillie_PSW import baillie_psw_test
import argparse
from random import getrandbits

def gen_safe_prime(size):
    if size < 3: return None
    size = size -1
    a = getrandbits(size) + (1 << size)
    while not baillie_psw_test(a) or not baillie_psw_test((a-1)//2):
        a = getrandbits(size) + (1 << size)
    return a
#function to check for negative int arguments
def check_positive(value):
    ivalue = int(value)
    if ivalue < 3:
         raise argparse.ArgumentTypeError("The size has to be above 3 bits")
    return ivalue

#what to do if the file is run as a script independently
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description = "Generate a safe prime number")
    parser.add_argument('s', metavar='s', type=check_positive, help='The size in bits that we want our prime to be')
    args = parser.parse_args()
    result = gen_safe_prime(args.s)
    print(result)
