import argparse
import gmpy2
from gmpy2 import mpz, f_mod, f_div
#the extended euclidean algorithm
def extended_euclidean(a, b):
	flag = False
	if b > a:
		a, b = b, a
		flag = True
	s0 = 1
	s1 = 0
	t0 = 0
	t1 = 1
	k = 0
	while b != 0:
		k+=1
		q = f_div(a, b)
		a, b = b, f_mod(a, b)
		s0, s1 = s1, s0 - q * s1
		t0, t1 = t1, t0 - q * t1
	#a is the gcd(a,b), s0 and t0 are the bezout coefficients
	if flag:
		return  int(a), int(t0), int(s0)
	else:
		return  int(a), int(s0), int(t0)

#what to do if the file is run as a script independently
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description = "The extended euclidean method, \
    	returns a tuple containing: gcd(a,b) and two bezout coefficients")
    parser.add_argument('a', metavar='a', type=int, help='An integer a')
    parser.add_argument('b', metavar='e', type=int, help='An integer b')
    args = parser.parse_args()
    result = extended_euclidean(args.a, args.b)
    print(result)