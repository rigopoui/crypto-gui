import argparse
from math import gcd
try:
    from scripts.baillie_PSW import baillie_psw_test
    from scripts.extended_euclidean import extended_euclidean
except ModuleNotFoundError:
    from extended_euclidean import extended_euclidean
    from baillie_PSW import baillie_psw_test
#looking for x where g^x = n (modulo p) and 1<g<p and p is a prime

#code from https://stackoverflow.com/questions/37439263/implementing-pollards-rho-algorithm-for-discrete-logarithms
#based on Pommerance's book on primes

def pollard_rho_discrete_log(g, n, p):
    if not baillie_psw_test(p):
        return str(p)+" is not a prime"
    if g >= p or n >=p:
        return "g and n have to be less than p"
    def f(xab):
        x, a ,b = xab[0], xab[1], xab[2]
        if 3*x < p:
            return (n * x) % p ,(a + 1) % (p-1), b
        elif 3*x > 2*p:
            return (g * x) % p, a, (b+1) % (p-1)
        return x*x % p, (2*a) % (p-1), (2*b) % (p-1)

    xab = (1, 0, 0)
    XAB = f((1, 0, 0))
    while xab[0] != XAB[0]:
        xab = f(xab)
        XAB = f(f(XAB))
    a = xab[1] - XAB[1]
    b = XAB[2] - xab[2]
    if b == 0: return None
    d = gcd(a, p - 1)
    if d == 1: return (b % (p - 1) * inverse(a % (p - 1), p - 1)) % (p - 1)
    m, l = 0, (b % ((p - 1) // d) * inverse(a % ((p - 1) // d),(p - 1) // d)) % ((p - 1) // d)
    while m < d:
        if pow(g,l,p) == n: return l
        m, l = m+1, (l+((p-1)//d))%(p-1)
    return None

#returns the inverse mod of a with b
def inverse(a, b):
    return extended_euclidean(a,b)[1] % b

#function to check for negative int arguments
def check_positive(value):
    ivalue = int(value)
    if ivalue < 0:
         raise argparse.ArgumentTypeError("%s is an invalid positive int value" % value)
    return ivalue

#what to do if the file is run as a script independently
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description = "Compute a discrete logarithm using \
    	Pollard's rho method")
    parser.add_argument('g', metavar='g', type=check_positive, help='the base number, has to be a generator of the group')
    parser.add_argument('n', metavar='n', type=check_positive, help='the number n which is the result of g^a= n (modulo p)')
    parser.add_argument('p', metavar='p', type=check_positive, help='the prime that determines the order of our cyclic group (order = p-1)')
    args = parser.parse_args()
    result = pollard_rho_discrete_log(args.g, args.n, args.p)
    print(result)
