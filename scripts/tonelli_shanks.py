import argparse
try:
	from scripts.baillie_PSW import baillie_psw_test
	from scripts.legendre_symbol import legendre_symbol
except ModuleNotFoundError:
	from baillie_PSW import baillie_psw_test
	from legendre_symbol import legendre_symbol
from gmpy2 import mpz, f_mod, f_div, powmod

#tonelli - shanks algorithm for finding a square root modulo p
def tonelli_shanks(n, p, to_cmd = False):
	n, p = mpz(n), mpz(p)
	if to_cmd:
		if not baillie_psw_test(p): return str(p)+" is not a prime number."
	if legendre_symbol(n, p) != 1:
		return str(n)+" is not a quadratic residue modulo "+str(p)
	Q = p-1
	S = 0
	while f_mod(Q, 2) == 0:
		Q = f_div(Q, 2)
		S += 1
	for z in range(2,p):
		if legendre_symbol(z, p) == -1: break
	M = S
	c = powmod(z, Q, p)
	t = powmod(n, Q, p)
	R = powmod(n, f_div((Q + 1), 2), p)
	while True:
		if t % p == 0: return 0
		if t % p == 1: return int(R % p), int(-R % p)
		t_i = t*t
		for i in range(1, M):
			if t_i % p == 1: break
			#if pow(t, pow(2, i), p) == 1: break
			t_i *= t_i
		b = pow(c, 1 << (M-i-1), p)
		M = i
		c = b*b
		t = t*c
		R = R*b
	
		

#function to check for negative int arguments
def check_positive(value):
	ivalue = int(value)
	if ivalue < 0:
		 raise argparse.ArgumentTypeError("%s is an invalid positive int value" % value)
	return ivalue

#what to do if the file is run as a script independently
if __name__ == "__main__":
	parser = argparse.ArgumentParser(description = "Finds a square root of n modulo p using the Tonelli - Shanks algorithm")
	parser.add_argument('n', metavar='n', type=check_positive, help='an integer n')
	parser.add_argument('p', metavar='p', type=check_positive, help='a prime p')
	args = parser.parse_args()
	result = tonelli_shanks(args.n, args.p, True)
	print(result)
