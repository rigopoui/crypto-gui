try:
    from scripts.baillie_PSW import baillie_psw_test
except ModuleNotFoundError:
    from baillie_PSW import baillie_psw_test
import argparse
from gmpy2 import mpz, is_square, isqrt, f_div

#n a positive odd composite integer to factor
def fermat_factorization(n):
	n = mpz(n)
	if baillie_psw_test(n):
		return n
	if n % 2 == 0:
		return "The number has to be odd"
	x = isqrt(n)
	t = 2*x + 1
	r = x*x - n
	#variable to count steps until a factor is found
	#g = 0
	while not is_square(r):
		r += t
		t += 2
		#g+=1
	#print(g)
	x = f_div((t-1), 2)
	y = isqrt(r)
	#v = x-y
	#another way to calculate the steps taken based on the factor
	#print(ceil((sqrt(n)-v)**2/(2*v)))
	return int(x-y), int(x+y)

#function to check for negative int arguments
def check_positive(value):
    ivalue = int(value)
    if ivalue < 0:
         raise argparse.ArgumentTypeError("%s is an invalid positive int value" % value)
    return ivalue

#what to do if the file is run as a script independently
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description = "Fermat's factorization method")
    parser.add_argument('n', metavar='n', type=check_positive, help='The number to factor')
    args = parser.parse_args()
    result = fermat_factorization(args.n)
    print(result)
