import argparse
try:
	from scripts.baillie_PSW import baillie_psw_test
except ModuleNotFoundError:
	from baillie_PSW import baillie_psw_test
import math

#looking for x where g^x = n (modulo p) and 1<g<p and p is a prime
def babystep_giantstep(g, n, p):
	if not baillie_psw_test(p): return str(p)+" is not a prime number."
	m = math.ceil(math.sqrt(p))
	pair_map = {}
	e = 1
	for j in range(0, m):
		pair_map[e] = j
		e = (e * g) % p
	inv_g = pow(g,p-m-1,p)
	k = n
	for i in range(0,m):
		if k in pair_map:
			return i*m + pair_map[k]
		k = (k * inv_g)% p
	return None

#function to check for negative int arguments
def check_positive(value):
	ivalue = int(value)
	if ivalue < 0:
		 raise argparse.ArgumentTypeError("%s is an invalid positive int value" % value)
	return ivalue

#what to do if the file is run as a script independently
if __name__ == "__main__":
	parser = argparse.ArgumentParser(description = "Compute a discrete logarithm using \
		shanks baybstep giantstep method")
	parser.add_argument('g', metavar='g', type=check_positive, help='the base number')
	parser.add_argument('n', metavar='n', type=check_positive, help='the number n which is the result of g^a= n (modulo p)')
	parser.add_argument('p', metavar='p', type=check_positive, help='the prime that determines the order of our cyclic group (order = p-1)')
	args = parser.parse_args()
	result = babystep_giantstep(args.g, args.n, args.p)
	print(result)
