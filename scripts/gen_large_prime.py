try:
    from scripts.baillie_PSW import baillie_psw_test
except ModuleNotFoundError:
    from baillie_PSW import baillie_psw_test
import argparse
from random import getrandbits

def gen_large_prime(size):
    if size == 1: return None
    size = size -1
    a = getrandbits(size) + (1 << size)
    while not baillie_psw_test(a): 
        a = getrandbits(size) + (1 << size)
    return a
#function to check for negative int arguments
def check_positive(value):
    ivalue = int(value)
    if ivalue < 0:
         raise argparse.ArgumentTypeError("%s is an invalid positive int value" % value)
    return ivalue

#what to do if the file is run as a script independently
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description = "Generate a large prime number")
    parser.add_argument('s', metavar='s', type=check_positive, help='The size in bits that we want our prime to be')
    args = parser.parse_args()
    result = gen_large_prime(args.s)
    print(result)
