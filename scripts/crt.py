import gmpy2
import argparse
try:
    from scripts.extended_euclidean import extended_euclidean
except ModuleNotFoundError:
    from extended_euclidean import extended_euclidean
import argparse
from gmpy2 import mpz,mpq,mpfr,mpc

def mpz_list(old_list):
	new_list = []
	for i in range(len(old_list)):
		new_list.append(mpz(old_list[i]))
	return new_list

def chinese_theorem(a , n):
	a = mpz_list(a)
	n = mpz_list(n)
	if len(a) != len(n):
		return "Input tuples have different lengths"
	for i in range(len(n)):
		if a[i] < 0 or n[i] < 1:
			return "Numbers in tuple have non allowed values"
		if a[i]>=n[i]:
			return "Not all a_i < n_i"
	for i in range(len(n)):
		for k in range(i+1, len(n)):
			if gmpy2.gcd(n[i], n[k]) != 1:
				return "Numbers in n tuple are not pairwise coprime"
	product = 1
	solution = 0
	for num in n:
		product *= num
	joined_lists = zip(a, n)
	for a_i , n_i  in joined_lists:
		M_i = gmpy2.f_div(product, n_i)	 
		y_i = extended_euclidean(M_i, n_i)[1]
		if y_i < 0:
			y_i += n_i
		solution += a_i*M_i*y_i
	return solution % product

		
def check_int_tuple(value):
	value = eval(value)
	if isinstance(value, tuple):
		for k in value:
			if not isinstance(k, int):		
				raise argparse.ArgumentTypeError("%s is an invalid as a tuple of integers" % value)
	return value

#what to do if the file is run as a script independently
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description = "Implementation of the Chinese remainder theorem")
    parser.add_argument('a', metavar='a', type=check_int_tuple, help='tuple of integers')
    parser.add_argument('n', metavar='n', type=check_int_tuple, help='tuple of integers ')
    args = parser.parse_args()
    result = chinese_theorem(args.a, args.n)
    print(result)