import argparse
import gmpy2
from gmpy2 import mpz

def pow_mod(b, e, N):
	e_bin = bin(e)[2:]
	d = 1
	b = mpz(b)
	N = mpz(N)
	#from n to 0 with step = -1
	for i in range(len(e_bin) - 1, -1, -1):
		if e_bin[i] == '1':
			d = gmpy2.f_mod((d*b), N)
		b = gmpy2.f_mod((b*b), N)
	return int(d)

#function to check for negative int arguments
def check_positive(value):
    ivalue = int(value)
    if ivalue < 0:
         raise argparse.ArgumentTypeError("%s is an invalid positive int value" % value)
    return ivalue

#what to do if the file is run as a script independently
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description = "Pow mod method, raise a number to a power, modulo another number")
    parser.add_argument('a', metavar='a', type=int, nargs='?', 
                        help='The number to raise to some power')
    parser.add_argument('e', metavar='e', type=check_positive, help='The exponent')
    parser.add_argument('N', metavar='N', type=int, help='The number to find mudulo by')
    args = parser.parse_args()
    result = pow_mod(args.a, args.e, args.N)
    print(result)