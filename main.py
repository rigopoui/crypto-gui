import gi
import os
import signal
import json
import argparse
import importlib
import logging
import sys
from functools import wraps
from scripts.baillie_PSW import baillie_psw_test
import multiprocessing as mp
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, GLib

#shows an error message dialog
def spawn_error_dialog(parent, exception):
		title = exception.__class__.__name__
		if title == 'str':
			error = Gtk.MessageDialog(parent = parent, flags = 0, 
			message_type = Gtk.MessageType.ERROR, buttons = Gtk.ButtonsType.OK, text = exception, title = 'Error')
		else:
			text = str(exception)
			error = Gtk.MessageDialog(parent = parent, flags = 0, 
				message_type = Gtk.MessageType.ERROR, buttons = Gtk.ButtonsType.OK, text = text, title = title)
		error.run()
		error.destroy()

def launch_window(locale, locales, maximized = False, size = None, position = None):
	global window
	window = MyWindow(locale, locales)
	#setting the default and minimum window sizes
	if maximized:
		window.maximize()
	elif size != None and position != None:
		window.resize(size[0], size[1])
		window.move(position[0], position[1])
	else:
		window.set_default_size(1050, 650)
	window.set_size_request(750, 420)
	window.connect("destroy", Gtk.main_quit)
	window.show_all()
	Gtk.main()
	return window	

#function to change language
def change_locale(menu_item, old_window, timeout):
	is_maximized = old_window.is_maximized()
	size = old_window.get_size()
	position = old_window.get_position()
	old_window.stop_running_processes(None)
	new_locale = menu_item.get_label()
	if old_window.locale == new_locale:
		return
	GLib.source_remove(timeout)
	old_window.destroy()
	launch_window(new_locale, locales, is_maximized, size, position)
	#Gtk.main_quit()

	#window.__init__(new_locale, locales)

#our custom window class
class MyWindow(Gtk.Window):
	def __init__(self, locale, locales):
		#first we open our data/"locale" folder 
		self.locale = locale
		self.data_path = "data/"+locale+"/"
		self.data_files = []
		for file in os.listdir(self.data_path):
			if file.endswith(".json"):
				self.data_files.append(file)
		try:
			#then we read the strings file, this file contains all the strings displayed in the UI
			#json files are loaded as dicts in python so it's easy to use
			with open(self.data_path+"strings.json", 'r') as f:
				self.program_strings = json.load(f)
		except (FileNotFoundError, IOError, json.decoder.JSONDecodeError) as e:
			spawn_error_dialog(self, "Error trying to decode strings.json")
			sys.exit(0)
		Gtk.Window.__init__(self, title=self.program_strings['program_title'])

		self.set_default_icon_from_file("icon.png")

		#made a top-level menu_box just for the menu bar because otherwise there is spacing between it 
		#and the rest of the main_box contents
		self.menu_box = Gtk.Box(orientation = Gtk.Orientation.VERTICAL, spacing = 0)
		self.add(self.menu_box)

		#attatching a timeout to our update function
		self.update_timeout = GLib.timeout_add(100, self.update_results) 

		#Basic menu bar
		self.main_menu_bar = Gtk.MenuBar()
		self.menu_box.pack_start(self.main_menu_bar, False, False, 0)
		self.language_item = Gtk.MenuItem.new_with_label(self.program_strings["language"])
		lang_menu = Gtk.Menu()
		self.language_item.set_submenu(lang_menu)
		for lang in locales:
			item = Gtk.MenuItem.new_with_label(lang)
			lang_menu.add(item)
			item.connect("activate", change_locale, self, self.update_timeout)
			#item.connect("activate", self.change_locale)

		#adding items for our menu bar
		help_item = Gtk.MenuItem.new_with_label(self.program_strings["help"])
		help_menu = Gtk.Menu()
		help_item.set_submenu(help_menu)
		about_item = Gtk.MenuItem.new_with_label(self.program_strings["about"])
		about_item.connect("activate", self.show_about_dialog)
		help_menu.add(about_item)
		self.main_menu_bar.add(self.language_item)
		self.main_menu_bar.add(help_item)

		#first box. it holds two items, the central horizontal box, and the results text view
		self.main_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing = 6)
		self.menu_box.pack_start(self.main_box, True, True, 0)
		
		#this is the box that contains most of the UI elements, it is packed inside main_box
		self.central_horizontal_box = Gtk.Box(spacing = 2)
		self.main_box.pack_start(self.central_horizontal_box, True, True, 0)

		#this is the list on the left that lists all the available functions
		self.scrolling_tree_view = Gtk.ScrolledWindow()
		self.scrolling_tree_view.set_policy(Gtk.PolicyType.NEVER, Gtk.PolicyType.ALWAYS)
		#cell renderer required for the tree view
		cell_renderer = Gtk.CellRendererText()
		cell_renderer.set_padding(10,5)

		#column to display the program categories in the tree view
		column_category = Gtk.TreeViewColumn(self.program_strings['programs'], cell_renderer, text = 0)

		#The tree model holds a str which is the name of a category or the name of a program
		#and an int which is the index of self.data[] on which the corresponding program's json data is stored
		self.tree_model = Gtk.TreeStore(str, int)
		self.func_tree_view = Gtk.TreeView(model = self.tree_model)
		self.scrolling_tree_view.set_size_request(200,0)
		self.func_tree_view.append_column(column_category)
		self.func_tree_view.connect('cursor-changed', self.on_cursor_changed)

		#making the tree view sorted
		column_category.set_sort_column_id(0)
		self.tree_model.set_sort_column_id(0, Gtk.SortType.ASCENDING)
		self.scrolling_tree_view.add(self.func_tree_view)

		#adding the function list to the main box, on the leftside
		self.central_horizontal_box.pack_start(self.scrolling_tree_view, False, False, 0)

		#the box on the right that contains all the script-specific UI stuff 
		self.right_vertical_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing = 6)
		self.right_vertical_box.set_size_request(20, 200)
		self.right_vertical_box.set_margin_start(20)
		self.right_vertical_box.set_margin_end(20)
		self.right_vertical_box.set_margin_top(10)
		self.right_vertical_box.set_margin_bottom(10)
		self.central_horizontal_box.pack_start(self.right_vertical_box, True, True, 0)		

		#a button box that sits on the bottom and contains the execute button, clear text button, etc.
		self.button_box = Gtk.Box(orientation = Gtk.Orientation.HORIZONTAL, 
			spacing = 6, halign = Gtk.Align.END, valign = Gtk.Align.END)

		#a spinner to show if a script process is running
		self.process_spinner = Gtk.Spinner(halign = Gtk.Align.START)
		self.button_box.pack_start(self.process_spinner, False, False, 0)

		#a button that stops all currently running processes
		self.stop_button = Gtk.Button.new_from_icon_name("window-close", Gtk.IconSize.BUTTON)
		self.stop_button.connect("clicked", self.stop_running_processes)
		self.stop_button.set_tooltip_text(self.program_strings["stop_running_programs_button_tooltip"])
		self.stop_button.set_sensitive(False)
		self.button_box.pack_start(self.stop_button, False, False, 0)

		#a button to clear the text from the result text view
		self.clear_text_button = Gtk.Button.new_from_icon_name("edit-clear", Gtk.IconSize.BUTTON)
		self.clear_text_button.connect("clicked", self.delete_text)
		self.clear_text_button.set_sensitive(False)
		self.clear_text_button.set_tooltip_text(self.program_strings["clear_text_button_tooltip"])
		self.button_box.pack_start(self.clear_text_button, False, False, 0)

		#a button to save the output as a text file
		self.save_results_button = Gtk.Button.new_from_icon_name("document-save-as", Gtk.IconSize.BUTTON)
		self.save_results_button.connect("clicked", self.save_results)
		self.save_results_button.set_sensitive(False)
		self.save_results_button.set_tooltip_text(self.program_strings["save_results_button_tooltip"])
		self.button_box.pack_start(self.save_results_button, False, False, 0)

		#a button to execute a script
		self.execute_button = Gtk.Button.new_from_icon_name("media-playback-start", Gtk.IconSize.BUTTON)
		self.execute_button.connect("clicked", self.on_execute_clicked)
		self.execute_button.set_tooltip_text(self.program_strings["execute_button_tooltip"])
		self.button_box.pack_start(self.execute_button, False, False, 0)

		#shows us which function is currently selected inside the func_tree_view
		self.func_index = 0

		#holds all json data for selected locale
		self.data = []
		
		#this dictionary will hold the name of the category + the iter of its TreeStore
		#so you can add children to it
		self.category_map = {}

		#a set to hold the pid of all running processes
		self.processes = set()

		#a queue for receiving the results of our scripts running on separate processes
		self.results_queue = mp.Queue()

		#will hold the entry objects which correspond to the arguments of the 
		#currently selected program
		self.args = []

		#a counter that we add to our tree view to easily get our data from the self.data
		#file when a program is selected from the func_tree_view
		file_index_counter = 0
		if len(self.data_files) == 1:
			spawn_error_dialog(self, "No compatible json files found, quitting the program")
			sys.exit(0)
		for file in self.data_files:
			if file =="strings.json":
				continue
			with open(self.data_path+file,'r') as f:
				try:
					file_data = json.load(f)
				except json.decoder.JSONDecodeError as e:
					spawn_error_dialog(self, "Error trying to decode "+file+".json file")
					sys.exit(0)
				self.data.append(file_data)
				
			if file_data["category"] not in self.category_map:
				#if the category doesn't exist yet, I add it, and on the int I put -1 to signal that this cell represents 
				#a category, not a program
				self.category_map[file_data["category"]] = self.tree_model.append(None, 
					[file_data["category"], -1])
			row = self.tree_model.append(self.category_map[file_data["category"]], 
				[file_data["title"], file_index_counter])
			file_index_counter += 1
		self.func_tree_view.expand_all()

		#scrolling window for the results text view
		self.scrolling_text_view = Gtk.ScrolledWindow()
		self.results_text_view = Gtk.TextView()
		self.results_text_view.set_wrap_mode(Gtk.WrapMode.CHAR)
		self.scrolling_text_view.set_policy(Gtk.PolicyType.NEVER, Gtk.PolicyType.ALWAYS)
		self.results_text_view.set_editable(False)
		self.scrolling_text_view.add(self.results_text_view)
		self.scrolling_text_view.set_size_request(0, 200)

		#connecting function for autoscrolling our scroll view - Gtk has a bug, waiting for fix
		#instead i have added the scroll to the update_results function
		#self.scrolling_text_view.connect('size-allocate', self.auto_scroll)
		self.main_box.pack_start(self.scrolling_text_view, False, False, 0)
		

	#simple function to remove all child widgets from a widget.
	#it is used when another program is selected from the list to clear the 
	#container right_vertical_box
	def remove_children(self, widget):
		children = widget.get_children()
		for child in children:
			widget.remove(child)

	#this function gets called when the user selects a program from the list
	#it is connected to the widget func_tree_view
	def on_cursor_changed(self, tree_view):
		self.args = []
		tree_iter = tree_view.get_selection().get_selected()[1]
		self.func_index = self.tree_model.get_value(tree_iter, 1)
		self.remove_children(self.right_vertical_box)
		if self.func_index == -1:
			label = Gtk.Label()
			label.set_text(self.program_strings["select_a_program"])
			label.set_justify(Gtk.Justification.CENTER)
			label.set_halign(Gtk.Align.CENTER)
			label.set_margin_bottom(60)
			label.set_line_wrap(True)
			label.set_valign(Gtk.Align.CENTER)
			self.right_vertical_box.pack_start(label, True, True, 0)
			self.right_vertical_box.show_all()
			return
		file_data = self.data[self.func_index]
		label = Gtk.Label()
		label.set_text(file_data['title'])
		self.right_vertical_box.pack_start(label, False, False, 0)

		label = Gtk.Label()
		label.set_markup("<small>"+file_data['description']+"</small>")
		label.set_justify(Gtk.Justification.LEFT)
		label.set_line_wrap(True)
		label.set_margin_start(30)
		label.set_margin_end(30)
		self.right_vertical_box.pack_start(label, False, False, 0)

		for i in range(len(file_data['arguments'])):
			argbox = Gtk.Box(spacing = 20)
			label = Gtk.Label()
			label.set_line_wrap(True)
			label.set_justify(Gtk.Justification.LEFT)
			label.set_halign(Gtk.Align.START)
			label.set_text(file_data['arg_desc'][i])
			argbox.pack_start(label, False, False, 0)
			arg_entry = Gtk.Entry()
			#connecting the entry to a function that limits what characters can be in the input based on argument info from .json
			arg_entry.connect("insert_text", self.on_entry_insert, ", .-+/*/()1234567890")

			#connecting function that checks if the entry is empty to enable/disable the execute button
			arg_entry.connect("changed", self.on_entry_changed)

			#connecting a function to execute program when 'Enter' key is pressed
			arg_entry.connect("activate", self.on_entry_activated)
			arg_entry.set_halign(Gtk.Align.END)
			#arg_entry.set_size_request(20,10)
			arg_entry.set_valign(Gtk.Align.CENTER)
			self.args.append(arg_entry)
			argbox.pack_start(arg_entry, True, True, 0)
			self.right_vertical_box.pack_start(argbox, False, False, 0)
		#Execute button is disabled untill all arguments are filled with some value
		self.execute_button.set_sensitive(False)
		self.save_results_button.set_sensitive(False)
		self.right_vertical_box.pack_start(self.button_box, True, True, 0)
		self.right_vertical_box.show_all()
		#stop the signal from propagating
		tree_view.stop_emission_by_name("cursor-changed")

	#what to do when the execute/run button is clicked
	def on_execute_clicked(self, button, file_path = None):
		file_data = self.data[self.func_index]
		script_name = file_data["script_name"]
		try:
			#we try to import the module and function based on the data on the .json files
			script = importlib.import_module("scripts."+script_name)
			script_method = getattr(script, file_data["method_name"])
			script_arguments = [arg.get_text() for arg in self.args]
			#The message that will be displayed before the results of the script
			message = self.program_strings["result_of"]+" \""+file_data["title"]+"\" "+self.program_strings["with_arguments"]\
			+" ("+''.join([script_arguments[i]+', ' for i in range(len(script_arguments)-1)])+script_arguments[-1]+')'
			#we evaluate our arguments to turn them from str type to the appropriate type
			script_arguments = self.evaluate_args(file_data['arguments'], script_arguments)
			#the new process which will run our script
			p = mp.Process(target=self.execute_script, args=(message, self.results_queue, script_method, \
				script_arguments, file_path))
			p.start()
			#we add it to the list of process IDs
			self.processes.add(p.pid)
			self.process_spinner.set_tooltip_text(str(len(self.processes))+" "\
				+self.program_strings["spinner_tooltip"])
			self.process_spinner.start()
			self.stop_button.set_sensitive(True)
		except(FileNotFoundError, IOError, SyntaxError, ValueError) as e:
			#logger.error(e, exc_info=True)
			#spawn_error_dialog(self, "Error trying to run \"" + script_name + ".py\" file")
			spawn_error_dialog(self, e)

	#the function that executes the script. it is a static method that runs in its own process
	@staticmethod
	def execute_script(message, queue, script_method, script_arguments, file_path = None):
		result = message+":\n"+str(script_method(*script_arguments))
		queue.put((result, os.getpid(), file_path))

	#updates the results of the result text view, gets data from different processes
	def update_results(self):
		if not self.results_queue.empty():
			data = self.results_queue.get_nowait()
			result = data[0]
			process_id = data[1]
			file_path = data[2]
			if file_path == None:
				label_buffer = self.results_text_view.get_buffer()
				GLib.idle_add(label_buffer.insert, label_buffer.get_end_iter(), result+"\n")
				GLib.idle_add(self.auto_scroll, self.scrolling_text_view.get_vadjustment())
			else:
				try:
					with open(file_path, 'w') as file:
						file.write(data[0])
				except IOError as e:
						spawn_error_dialog(self, e)
			self.processes.remove(process_id)
			self.process_spinner.set_tooltip_text(str(len(self.processes))+" "\
				+self.program_strings["spinner_tooltip"])
			if len(self.processes) == 0: 
				self.process_spinner.stop()
				self.stop_button.set_sensitive(False)
			self.clear_text_button.set_sensitive(True)
		return True

	#converts the arguments from a list of strings to the appropriate type for our scripts
	def evaluate_args(self, arg_types, arg_list):
		new_arg_list=[]
		for i in range(len(arg_types)):
			evaled = eval(arg_list[i])
			if arg_types[i] == "pos_int":
				if isinstance(evaled, int) and evaled > 0:
					new_arg_list.append(evaled)
				else:
					raise ValueError("%s is not a positive integer" % arg_list[i])
			elif arg_types[i] == "pos_float":
				if isinstance(evaled, float) and evaled > 0:
					new_arg_list.append(evaled)
				else:
					raise ValueError("%s is not a positive floating point number" % arg_list[i])
			elif arg_types[i] == "int":
				if isinstance(evaled, int):
					new_arg_list.append(evaled)
				else:
					raise ValueError("%s is not an integer" % arg_list[i])
			elif arg_types[i] == "float":
				if isinstance(evaled, float):
					new_arg_list.append(evaled)
				else:
					raise ValueError("%s is not a floating point number" % arg_list[i])
			elif arg_types[i] == "tuple_int":
				if isinstance(evaled, tuple):
					for item in evaled:
						if not isinstance(item, int):
							raise ValueError("%s is not a tuple of positive integers" % arg_list[i])
					new_arg_list.append(evaled)
				else:
					raise ValueError("%s is not a tuple" % arg_list[i])
			elif arg_types[i] == "prime":
				if isinstance(evaled, int) and evaled > 0 and baillie_psw_test(evaled):
					new_arg_list.append(evaled)
				else:
					raise ValueError("%s is not a prime integer" % arg_list[i])
			else:
				new_arg_list.append(evaled)
		return new_arg_list

	#auto scrolls a scrollable label as text gets appended to it - bugged, using workaround
	#workaround scrolls almost to the botom, have to wait for fix from gtk
	"""def auto_scroll(self, widget, event, data=None):
		adj = widget.get_vadjustment()
		adj.set_value(adj.get_upper() - adj.get_page_size())"""
	@staticmethod
	def auto_scroll(adj):
		adj.set_value(adj.get_upper() - adj.get_page_size())

	#deletes all text from a label
	def delete_text(self, button):
		text_buffer = self.results_text_view.get_buffer()
		text_buffer.delete(text_buffer.get_start_iter(), text_buffer.get_end_iter())
		button.set_sensitive(False)

	#produces the save file dialog and calls the execute function
	def save_results(self, button):
		save_dialog = Gtk.FileChooserDialog(title = self.program_strings["save_file"], parent = self, 
			action = Gtk.FileChooserAction.SAVE)
		save_dialog.set_do_overwrite_confirmation(True)
		save_dialog.add_buttons(Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL, 
			Gtk.STOCK_SAVE, Gtk.ResponseType.ACCEPT)
		response = save_dialog.run()
		if response == Gtk.ResponseType.ACCEPT:
			label_buffer = self.results_text_view.get_buffer()
			data = label_buffer.get_text(label_buffer.get_start_iter(), label_buffer.get_end_iter(), True)
			file_path = save_dialog.get_filename()
			self.on_execute_clicked(None, file_path)
			save_dialog.destroy()
		save_dialog.destroy()

	#limits text that can be put into an entry
	#Using *data to pass the allowed symbols through the signal to this function
	def on_entry_insert(self, entry, text, length, position, *data):
		allowed_symbols = data[0]
		position = entry.get_position()
		result = ''
		for char in text:
			if char in allowed_symbols:
				result+=char
		if result != '':
			#the signal has to be blocked while we insert text, so we avoid infinite recursion
			entry.handler_block_by_func(self.on_entry_insert)
			entry.insert_text(result, position)
			entry.handler_unblock_by_func(self.on_entry_insert)
			new_pos = position + len(result)
			GLib.idle_add(entry.set_position, new_pos)
		#stop emmision of signal because it has been handled by this function
		entry.stop_emission_by_name("insert_text")

	#function for disabling/enabling the execute_button and save_results_button
	# when all arguments are empty/filled
	def on_entry_changed(self, entry):
		arg_text_lengths = [entry.get_text_length() for entry in self.args]
		if not all(arg_text_lengths):
			self.execute_button.set_sensitive(False)
			self.save_results_button.set_sensitive(False)
		else:
			self.execute_button.set_sensitive(True)
			self.save_results_button.set_sensitive(True)

	#function to "press" the execute button when the enter key is pressed while an entry is focused
	def on_entry_activated(self, entry):
		if self.execute_button.get_sensitive():
			self.on_execute_clicked(self.execute_button)
		else:
			return

	#produces and shows the about dialog
	def show_about_dialog(self, menu_item):
		about_dialog = Gtk.AboutDialog()
		about_dialog.set_program_name(self.program_strings['program_title'])
		about_dialog.set_comments(self.program_strings['comments'])
		about_dialog.set_license(''.join(self.program_strings['license']))
		about_dialog.set_logo_icon_name()
		about_dialog.set_website(self.program_strings['website'])
		about_dialog.set_copyright(self.program_strings['copyright'])
		about_dialog.run()
		about_dialog.destroy()

	#function that kills all running processes (of our program, not the whole OS :P)
	def stop_running_processes(self, button):
		for pid in self.processes:
			os.kill(pid, signal.SIGTERM)
		self.processes.clear()
		self.process_spinner.stop()
		self.process_spinner.set_tooltip_text("0 "+self.program_strings["spinner_tooltip"])
		self.stop_button.set_sensitive(False)
		
#what to do when the file is run from command line
if __name__ == "__main__":
	logger = logging.Logger('catch_all')
	locales = []
	try:
		for file in os.listdir("data/"):
			if os.path.isdir("data/" + file):
				locales.append(file)
	except Exception as e:
		spawn_error_dialog(None, e)
		sys.exit(0)
	parser = argparse.ArgumentParser(description = "crypto-algs main program")
	parser.add_argument('locale', metavar='locale', type=str, nargs='?', default = 'en', 
						help='Locale for the program. \
						Default is English(en).')
	args = parser.parse_args()
	locale = args.locale
	locales.sort()
	if locale not in locales:
		spawn_error_dialog(None, "Locale " + " \"" + locale + "\" not found, locale is set as \"en\"")
		locale = "en"
	window = None
	launch_window(locale, locales)